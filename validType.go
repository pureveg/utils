package utils

import (
	"strconv"
	"strings"
)

// check if the float64 is valid, with the definition of precision of integer and fractional part
func IsValidFloat(f float64) bool {
	l := strings.Split(strconv.FormatFloat(f, 'f', -1, 64), ".")
	if len(l[0]) > 11 {
		return false
	}
	if len(l[1]) > 8 {
		return false
	}
	return true
}
