package utils

import (
	"sync"
	"time"
)

var mu sync.Mutex

// it will generate unix nanoo asc, thread safe
func UnixNano() int64 {
	mu.Lock()
	defer mu.Unlock()
	return time.Now().UnixNano()
}
