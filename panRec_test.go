package utils 

import "log"

func rec(format string, f func(format string, args ...interface{})) {
	if e := recover(); e != nil {
		f(format, e)
	}
}

func pan(e interface{}) {
	if e != nil {
		panic(e)
	}
}

func ta() {
	defer rec("test a: %d", log.Printf)
	panic(1)
}

func tb() {
	defer func() {
		rec("test b: %d", log.Printf)
	}()
	panic(1)
}

func main() {
	ta()
	tb()
}
