package utils

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
)

const (
	salt = "Or#AcleSxdkLSdkjIDSFNSDLKNCKLNvisdnfklsooadkKDFSD"
)

// encrypt using hmac-sha512
func Encrypt(str string) string {
	h := hmac.New(sha512.New, []byte(str))
	h.Write([]byte(salt))
	return hex.EncodeToString(h.Sum(nil))
}

// check if they match
func IsEncryptedMatched(str, encrypted string) bool {
	return encrypted == Encrypt(str)
}
