package utils

import (
	l "log"
)

// throw panic and recover
func Panic(err interface{}) {
	if err != nil {
		panic(err)
	}
}

func Recover(format string, log func(format string, args ...interface{})) {
	if e := recover(); e != nil {
		if log != nil {
			log(format, e)
		} else {
			l.Printf(format, e)
		}
	}
}

/*
	Must use like A, B is incorrect

	A:
	func A() {
		defer Recover(format, logFunc)
		Panic(err)
	}

	B:
	func B() {
		defer func() {
			Recover(format, logFunc)
		}()
		Panic(err)
	}
*/
