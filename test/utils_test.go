package test

import (
	"container/list"
	"sync"
	"testing"
	u "bitbucket.org/pureveg/utils"
)

func Test_Encrypt(t *testing.T) {
	enc := u.Encrypt("Hello world")
	if u.IsEncryptedMatched("Hello world", enc) {
		t.Log("Encrypted matched")
	} else {
		t.Error("Not matched")
	}
}

func Test_UnixNano(t *testing.T) {
	l := list.New()
	var wg sync.WaitGroup
	for i := 0; i < 1e5; i++ {
		wg.Add(1)
		go func() {
			tmp := u.UnixNano()
			l.PushBack(tmp)
			wg.Done()
		}()
	}
	wg.Wait()

	// for e := l.Front(); e != nil; e = e.Next() {
	// 	t.Log(e.Value)
	// }

	e := l.Front()
	i := 0
	for {
		nextE := e.Next()
		if nextE == nil {
			break
		}
		if e.Value.(int64) > nextE.Value.(int64) {
			t.Errorf("Something is wrong, the %dth is %d, the %d is %d", i, e.Value.(int64), i+1, nextE.Value.(int64))
			break
		}
		e = nextE
	}
	t.Log("Done testing")
}
